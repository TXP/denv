
This guide describes the instructions to configure a user namespace in Docker

## Principle

- use a namespace to use the `root` user in docker containers more safely
- convert the `root` user inside your container to your host `user`

## Requirements

You will need:
- A Linux OS with a user namespace-ready kernel.
  To check that, look for a `CONFIG_USER_NS=y` in the kernel config file (usually in `/boot/config-<kernel version>`, `/boot/config.gz`, or `/proc/config.gz`),
- A "recent" Docker Engine version (1.10+). You can follow [these instructions](https://docs.docker.com/engine/installation/).

## Configuration

**NOTE**: first make sure your local user is in the `docker` group

```bash
$ sudo adduser <your user> docker
```

### Configure Docker to use a namespace

#### What is all this configuration for ?

Basically, the namespace will translate inside docker uid and gid with the table define in the namespace configuration (`/etc/subuid` and `/etc/subgid`).

This configuration sets the first uid (uid=0) to translate to your uid, and the following 65536 ones to 100000-165535.

This means whenever you create a file or launch a process in root inside the docker container, this will be interpreted by the system as your user outside of docker.

See [this post](https://www.linux.com/blog/learn/2017/8/hardening-docker-hosts-user-namespaces) and [Docker documentation about namespaces](https://docs.docker.com/engine/security/userns-remap/) to have a bit more of explanations.

#### Clean the docker images

It seems changing the docker namespace will also change where docker store the docker images (from `/var/lib/docker/overlay2` to `/var/lib/docker/<uid.gid>/overlay2`).

Therefore cleaning the locally stored images should be a good idea before changing the namespace.

```bash
$ docker rmi $(docker images -q)
```

#### Define a namespace

Get your uid:
```bash
$ id
uid=<your uid>(<your user>) <...>
```

Create a "user" that will be used exclusively for the namespace definition. For this example, we create an arbitrary `dockremap` user:
```
$ sudo adduser --disabled-password --disabled-login --no-create-home dockremap
```

Now define a `dockremap` namespace: add the the following lines in the respective files:

`/etc/subuid`:
```
dockremap:<your uid>:1
dockremap:100000:65536
```

`/etc/subgid`:
```
dockremap:<your uid>:1
dockremap:100000:65536
```

#### Set namespace in docker daemon

Edit the daemon configuration:
`/etc/docker/daemon.json`:
```
{
    "userns-remap": "dockremap"
}
```

Then restart the daemon
```bash
$ sudo service docker restart
```

#### Check your namespace configuration

Here is an example of check:
```bash
$ mkdir /tmp/test1 # create a test directory

$ docker run --rm -it -v /tmp/test1:/tmp/test1 debian:latest /bin/bash # launch a docker with the directory mounted

root@<inside_the_container>:/# touch /tmp/test1/mytralala && ls -aln /tmp/test1/mytralala
-rw-r--r-- 1 0 0 0 Jan 16 16:17 /tmp/test1/mytralala # uid=gid=0

$ ls -al /tmp/test1/mytralala # outside of container
-rw-r--r-- 1000 1000 0 Jan 16 17:18 /tmp/test1/mytralala # outside of the container, uid=gid=1000 
```

## Usage

**NOTE**: I strongly suggest to:
- Do not mount your whole system tree inside the docker container, as it can be harmful
- Mount a "workspace" directory on the same absolute path inside the docker container: this way, paths generated inside the container will work outside (ie `--prefix` will work)

```bash
$ export WORKSPACE=/path/to/your/project/
$ docker run --rm -it -v "${WORKSPACE}:${WORKSPACE}" <the_good_docker_image>:tag /bin/bash
```

## Further read

See [this post](https://www.jujens.eu/posts/en/2017/Jul/02/docker-userns-remap/) for more details
