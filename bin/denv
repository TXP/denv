#!/bin/sh
# denv = docker+env: Use docker as a local build environment

# TODO
# add docker-compose options to command

BASEDIR=$(dirname $(realpath "$0"))

DENV_DIR=${DENV_DIR:="$BASEDIR/.."}
DENV_LOCAL_ENV_PROFILE=$DENV_DIR/local_env.yml
DENV_PROFILES_DIR=$DENV_DIR/profiles

_log_info()
{
    local green="\033[1;32m"
    local normal="\033[0m"
    echo "[${green}INFO${normal}] $1" 1>&2
}

_log_warning()
{
    local red="\033[1;33m"
    local normal="\033[0m"
    echo "[${red}WARNING${normal}] $1" 1>&2
}


# Get "real" latest tag from a private repository
# Explicitely avoid "latest" tag if possible
# Assumes the sort function sorts in the correct order
_get_latest_tag_from_private_repo()
{
    local repo=$1
    local image=$2
    curl -X GET "https://${repo}/v2/${image}/tags/list" 2>/dev/null \
        | sed 's;";;g' \
        | grep -o 'tags:\[[^\]*]' \
        | sed 's;tags:\[\([^\]*\)];\1;' \
        | tr ',' '\n' \
        | grep -v "latest" \
        | sort | tail -n 1
}

_denv_usage()
{
    exported_env_variables=$1
    cat << EOF

NAME:
    denv - Docker as a local development environment

USAGE:
    denv <command> [<args>]

COMMANDS:
    help
        display this message

    list
        list all available profiles.

    <profile> [<exported_directory>] [<docker-compose run args>]
        run a docker-compose profile with the appropriate exported directory.
        <exported_directory> and <docker-compose run args> are optional.

        The exported directory is selected as follows:
        If you specify an <exported_directory> after the profile name, <exported_directory> will be exported.
        If the WORKSPACE environment variable is defined and not empty, \$WORKSPACE will be exported.
        Otherwise the currect directory will be exported.

NOTE:
    denv is not meant to export directly your HOME directory, at least because containers can write files with different users and groups owners.
    It is strongly suggested to not launch denv directly from your HOME directory without specifying an exported directory.

EOF
}

_denv_list_profiles()
{
    for p in $DENV_PROFILES_DIR/*.yml; do
        echo "$(basename $p | sed 's/\.yml//g')"
    done
}

## Command parsing

if [ -z "$1" ] || [ "$1" = "help" ]; then
    echo "$(_denv_usage)"
    exit 0
fi

# list mode 
if [ "$1" = "list" ]; then
    echo "$(_denv_list_profiles)"
    exit 0
fi

# Get the profile name
DENV_PROFILE_FILE=$DENV_PROFILES_DIR/$1.yml
shift # remove $1 in arguments

# Handle workspace
workspace=$(pwd)
# If the new $1 is a directory, export this directory in the container
if [ -n "$1" ]; then
    exported_dir=$(realpath "$1")
fi
if [ -d "${exported_dir}" ]; then
    workspace=${exported_dir}
    _log_info "Exporting requested directory (${workspace}) inside the container"
    shift
elif [ -n "${WORKSPACE}" ]; then
    workspace=${WORKSPACE}
    _log_info "Exporting WORKSPACE (${workspace}) inside the container"
else
    _log_info "Exporting current directory (${workspace}) inside the container"
fi

if [ "${workspace}" = "${HOME}" ]; then
    _log_warning "Exporting \$HOME (${HOME}) inside the container. Please consider exporting another dorectory, by declaring a directory in the command line, defining a WORKSPACE environement variable, or launching denv from another directory."
fi

# DENV_WORKSPACE is mounted in the docker-compose files
export DENV_WORKSPACE=$workspace

# VLC specific: replace the "VLC_LATEST" with the correct tag
image_tag_with_private_latest=$(grep \$VLC_LATEST $DENV_PROFILE_FILE | grep "image:" | sed "s;^\s\+image:\s\+\(.\+\):\$VLC_LATEST\s*$;\1;")

# try to get the "real" latest tag in VLC images
tag=""
if [ "x${image_tag_with_private_latest}" != "x" ]; then
    _log_info "Detecting VLC_LATEST tag in profile"
    repo="$(echo $image_tag_with_private_latest | tr "/" "\n" | head -n1)"
    image="$(echo $image_tag_with_private_latest | tr "/" "\n" | tail -n1)"
    latest_tag=$(_get_latest_tag_from_private_repo $repo $image)
    if [ "x${latest_tag}" != "x" ]; then
        _log_info "Using Docker image ${image} with tag ${latest_tag}"
        export VLC_LATEST=${latest_tag}
    else
        _log_warning "Unable to find the appropriate Docker image ${image} tag, using the local copy of it"
        export VLC_LATEST=latest
    fi
fi

podman-compose -f "${DENV_LOCAL_ENV_PROFILE}" -f "${DENV_PROFILE_FILE}" run --rm --service-ports app $@

